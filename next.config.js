/**
 * @type {import('next').NextConfig}
 */
module.exports = {
  output: 'export',
  pageExtensions: ['md', 'mdx', 'ts', 'tsx'],
  reactStrictMode: true,
  trailingSlash: true,
}
