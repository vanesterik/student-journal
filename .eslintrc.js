module.exports = {
  extends: ['eslint-config-custom', 'next/core-web-vitals'],
  ignorePatterns: [
    '.eslintrc.cjs',
    'postcss.config.cjs',
    'tailwind.config.cjs',
    'vite.config.ts',
  ],
  parserOptions: {
    project: ['./tsconfig.json'],
    tsconfigRootDir: __dirname,
  },
  root: true,
  rules: {
    'fp/no-mutation': 'off',
    'fp/no-rest-parameters': 'off',
  },
}
