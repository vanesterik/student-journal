---
date: 2023-10-02T10:00:00+02:00
title: Statistics I
---

| Lecturers                                    |
| -------------------------------------------- |
| [Ilona Wilmont](mailto:ilona.wilmont@han.nl) |
| [Raoul Grouls](mailto:raoul.grouls@han.nl)   |

## Distributions

A probability distribution is: a mathematical description of a random phenomenon in terms of all its possible outcomes and their associated probabilities.

- If chance is involved - it is perse not deterministic.
- Typically plot the possible outcomes across the x-axis and plot the associated probabilities across the y-axis

### Discrete distributions

When an outcome can only take discrete values (ie. number of birds)

- Used with a bar graph based on fixed points at the x-axis
- Used when one is able to count the probability

A description of a discrete distribution is: Probability Mass Function. PMF (Probability Mass Function) has the following the functions:

- An event cannot have a negative probability (because something cannot happen less then never)
- The sum of probabilities of all events must be 1 (because something cannot happen more then always)
- The probability of a subset X of outcomes T is the same as adding the probabilities of the individual elements

#### Mathematical description

The probability is a function f over the sample space ...

#### Different types of discrete distributions

- Bernoulli distribution
- Uniform distribution
- Poisson distribution (required a lambda value - ie. average rate of events)

### Continuous distributions

When outcomes takes continuous values (ie. blood pressure)

- Used with a "squiggly" line
- Used when one is able to measure the probability

A description of a continuous distribution is: Probability Density Function. PDF (Probability Density Function) has the following the functions:

- $f(x)>0,VxEX$
- The integral of the probabilities of all possible events must be 1 (area under the curve)
- The probability of X of values in the interval

#### Different types of continuous distributions

- Normal distribution
- Log-normal distribution
- Exponential distribution (connected to Poisson distribution)
- Beta distribution

### Normal distribution

- Mean and standard deviation are required to create a normal distribution

## Modeling

### Motivation

### Epistemic vs Aleatoric

### Modeling under uncertainty
