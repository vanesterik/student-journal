---
date: 2023-11-13T10:00:00+02:00
title: Dimensionality Reduction
---

| Lecturers                                  |
| ------------------------------------------ |
| [Raoul Grouls](mailto:raoul.grouls@han.nl) |

## Topics

### Curse of Dimensionality

The **Curse of Dimensionality** refers to various phenomena that arise when analyzing and organizing data in high-dimensional spaces (often with hundreds or thousands of dimensions) that do not occur in low-dimensional settings such as the three-dimensional physical space of everyday experience. [https://en.wikipedia.org/wiki/Curse_of_dimensionality](https://en.wikipedia.org/wiki/Curse_of_dimensionality)

#### Manifold

A **manifold** is a topological space that locally resembles Euclidean space near each point. More precisely, each point of an n-dimensional manifold has a neighbourhood that is homeomorphic to the Euclidean space of dimension n. [https://en.wikipedia.org/wiki/Manifold](https://en.wikipedia.org/wiki/Manifold)

#### Big O Notation

**Big O notation** is a mathematical notation that describes the limiting behavior of a function when the argument tends towards a particular value or infinity. [https://en.wikipedia.org/wiki/Big_O_notation](https://en.wikipedia.org/wiki/Big_O_notation)

#### No Free Lunch Theorem

The **No Free Lunch Theorem** for optimization states that if an algorithm performs well on a certain class of problems then it necessarily pays for that with degraded performance on the set of all remaining problems. [https://en.wikipedia.org/wiki/No_free_lunch_theorem](https://en.wikipedia.org/wiki/No_free_lunch_theorem)

#### Heuristic

A **heuristic** technique, often called simply a heuristic, is any approach to problem solving or self-discovery that employs a practical method that is not guaranteed to be optimal, perfect or rational, but is nevertheless sufficient for reaching an immediate, short-term goal or approximation. [https://en.wikipedia.org/wiki/Heuristic](https://en.wikipedia.org/wiki/Heuristic)

### PCA

**Principal Component Analysis** (PCA) is a technique for reducing the dimensionality of datasets, increasing interpretability but at the same time minimizing information loss. [https://en.wikipedia.org/wiki/Principal_component_analysis](https://en.wikipedia.org/wiki/Principal_component_analysis)

$$
Eigenvalue for PC1 = \frac{SS(distances for PC1)}{n-1}
$$

The whole point of PCA is finding a new coordinate system that is more sufficient in order to describe the data. The new coordinate system is defined by the principal components. The first principal component is the direction in which the data varies the most. The second principal component is the direction in which the data varies the second most, and so on. The principal components are orthogonal to each other.

> One can reduce features because there is redundant information within a typical dataset

#### Matrix (2D-tensor)

A **matrix** is a 2D-tensor. A tensor is a generalization of vectors and matrices to potentially higher dimensions. Internally, tensors are represented as n-dimensional arrays of base datatypes. [https://www.tensorflow.org/guide/tensor](https://www.tensorflow.org/guide/tensor)

### t-SNE

**t-Distributed Stochastic Neighbor Embedding** (t-SNE) is a machine learning algorithm for visualization based on Stochastic Neighbor Embedding. It is a nonlinear dimensionality reduction technique well-suited for embedding high-dimensional data for visualization in a low-dimensional space of two or three dimensions. Specifically, it models each high-dimensional object by a two- or three-dimensional point in such a way that similar objects are modeled by nearby points and dissimilar objects are modeled by distant points with high probability. [https://en.wikipedia.org/wiki/T-distributed_stochastic_neighbor_embedding](https://en.wikipedia.org/wiki/T-distributed_stochastic_neighbor_embedding)

In a nutshell:

- A high dimensional dataset $X=\{x_1 ... x_n|x \epsilon \R^n\}$
- A low-dimensional mapping $Y=\{y_1 ... y_n|y \epsilon \R^d\}$
- The conditional probability $p_{j|i}$ that $x_i$ would pick $x_j$ as a neighbor
- The conditional probability $q_{j|i}$ that $y_i$ would pick $y_j$ as a neighbor
- A way to minimize the mismatch between P and Q

> If it's close in the high dimensional space, I want to have it close in the low dimensional space.

#### Kernel trick

The **kernel trick** is a method of using a linear classifier, such as a support vector machine (SVM), to solve a non-linear problem by mapping the original non-linear observations into a higher-dimensional space, where the linear classifier can be applied. [https://en.wikipedia.org/wiki/Kernel_method](https://en.wikipedia.org/wiki/Kernel_method)

> Using scissors to cut a folded towel in order to represent a non-linear relationship in a linear way.

#### Category theory

**Category theory** formalizes mathematical structure and its concepts in terms of a labeled directed graph called a category, whose nodes are called objects, and whose labelled directed edges are called arrows (or morphisms). A category has two basic properties: the ability to compose the arrows associatively, and the existence of an identity arrow for each object. [https://en.wikipedia.org/wiki/Category_theory](https://en.wikipedia.org/wiki/Category_theory)

## Learning Goals

- Q. What is the curse of dimensionality?  
  A. When having a dataset with a lot of features, the corresponding dimensions would consist of an enormous space of possible combination. This would make it very hard to find a good solution.

- Q. Understand what the motivation is for using dimensionality reduction techniques?  
  A. We need to apply dimensionality reduction to:

  1. speed up training process
  2. find a good solution by reducing the number of features
  3. visualize complex data in 2D or 3D

- Q. What is a manifold?  
  A. A definition of a manifold is a 2D sample of 3D object - ie. the surface of a table. The table is a 3D object, but the surface is a 2D manifold. In other words a manifold is an abstraction of a high dimensional object.

- Q. What is the manifold assumption or manifold hypothesis?  
  A. The manifold assumption holds that most high-dimensional datasets lie close to a much lower-dimensional manifold. This assumption is very often empirically observed.

### PCA

- Q. Understand how minimising the distance from a point to the projection onto a line is the same as maximising the distance from the projected point to the origin  
  A. ...

- Q. Understand how a Principal Component is created from a linear combination of variables, and how the "strength" of each variable adding to a PC can be varied  
  A. ...

- Q. Understand how the eigenvector relates to a Principal Component  
  A. ...

- Q. Understand how the eigenvalue is calculated
  A. ...

- Q. Understand what it means, intuitively, if the eigenvalue is lower or higher for an eigenvector.  
  A. ...

- Q. Understand what it means to select Principal Components that it accounts for a certain amount of variation in the data  
  A. ...

- Q. Understand what the downsides and upsides are of picking the top-k Principal Components  
  A. Downside of PCA is that it is a linear transformation. It is not able to capture non-linear relationships.

### t-SNE

- Q. What problem tries t-SNE to fix?  
  A. ...

- Q. Where does the "t" in t-SNE refer to, and why is that which it refers to useful?  
  A. ...

- Q. Understand that t-SNE tries to learn a d-dimensional mapping of the data of the data, typically with d as 2 or 3.  
  A. ...

- Q. Understand the global differences between PCA and t-SNE. What are upsides or downsides to t-SNE?  
  A. ...

- Q. How is the similarity calculated between the high dimensional points used to order the low dimensional objects?  
  A. ...

## Terms

### Stochastic component

The **stochastic component** is a component that is random. [https://en.wikipedia.org/wiki/Stochastic](https://en.wikipedia.org/wiki/Stochastic)

### Infinitesimal

An **infinitesimal** is a quantity that is close to zero. [https://en.wikipedia.org/wiki/Infinitesimal](https://en.wikipedia.org/wiki/Infinitesimal)

### Euclidean data

**Euclidean data** is data that is in a Euclidean space. A Euclidean space is a space in which every point can be described by a set of coordinates. [https://en.wikipedia.org/wiki/Euclidean_space](https://en.wikipedia.org/wiki/Euclidean_space)

### Transpose

The **transpose** of a matrix is an operator which flips a matrix over its diagonal; that is, it switches the row and column indices of the matrix A by producing another matrix, often denoted by $A^T$. [https://en.wikipedia.org/wiki/Transpose](https://en.wikipedia.org/wiki/Transpose)

## References

- [https://www.youtube.com/watch?v=FgakZw6K1QQ](https://www.youtube.com/watch?v=FgakZw6K1QQ)
- [https://www.youtube.com/watch?v=NEaUSP4YerM](https://www.youtube.com/watch?v=NEaUSP4YerM)
- [https://www.desmos.com/](https://www.desmos.com/)
