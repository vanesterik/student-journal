---
date: 2023-10-09T10:00:00+02:00
title: Statistics II
---

| Lecturers                                    |
| -------------------------------------------- |
| [Ilona Wilmont](mailto:ilona.wilmont@han.nl) |
| [Raoul Grouls](mailto:raoul.grouls@han.nl)   |

## Most Basic Statistical Models

- Linear
- Decision Tree - aka binary tree
- Random Forest
- XGBoost - ternary decision tree that is able to digest a NaN value

> Develop an intuition of all models in order to know which one to use.

## P-value

**P-value** is the probability of obtaining the observed results of a test, assuming that the null hypothesis is correct. [https://en.wikipedia.org/wiki/P-value](https://en.wikipedia.org/wiki/P-value)

**P-hacking** is the misuse of data analysis to find patterns in data that can be presented as statistically significant, thus dramatically increasing and overstating the significance of the results. [https://en.wikipedia.org/wiki/Data_dredging](https://en.wikipedia.org/wiki/Data_dredging)

> Probably you will never to use p-value in machine learning. But it will be good to know what it is and how it is used.

## Bayesian Inference

**Bayesian Inference** is a method of statistical inference in which Bayes' theorem is used to update the probability for a hypothesis as more evidence or information becomes available. [https://en.wikipedia.org/wiki/Bayesian_inference](https://en.wikipedia.org/wiki/Bayesian_inference)

> Consider pitting P-value statistics against Bayesian Inference.

> Data science is not just mathematics. It is also about the practical application and interpretation of results - think of the penguins dataset in combination with normal distribution, like the size of penguins starts and stops at some point ... where the normal distribution does only consider the theory of the distribution and not the practical application.

## Visual Testing

Visually test with QQ-plots and histograms if the data is normally distributed. Try to different distributions to see which one fits best:

- [https://en.wikipedia.org/wiki/Normal_distribution](https://en.wikipedia.org/wiki/Normal_distribution)
- [https://en.wikipedia.org/wiki/Skew_normal_distribution](https://en.wikipedia.org/wiki/Skew_normal_distribution)

## Kolmogorov–Smirnov Test

**Kolmogorov–Smirnov Test** is a nonparametric test of the equality of continuous, one-dimensional probability distributions that can be used to compare a sample with a reference probability distribution (one-sample K–S test), or to compare two samples (two-sample K–S test). [https://en.wikipedia.org/wiki/Kolmogorov%E2%80%93Smirnov_test](https://en.wikipedia.org/wiki/Kolmogorov%E2%80%93Smirnov_test)

## Beta Distribution

**Beta Distribution** is a family of continuous probability distributions defined on the interval [0, 1] parametrized by two positive shape parameters, denoted by α and β, that appear as exponents of the random variable and control the shape of the distribution. [https://en.wikipedia.org/wiki/Beta_distribution](https://en.wikipedia.org/wiki/Beta_distribution)

> One can state that applying statistics is an effort to reduce uncertainty.

Try to figure out how Beta distribution works well with Bernoulli distribution.

## Research Paradigm

**Research Paradigm** is a set of common beliefs and agreements shared between scientists about how problems should be understood and addressed. [https://en.wikipedia.org/wiki/Research_paradigm](https://en.wikipedia.org/wiki/Research_paradigm)

## Interpretivism vs Positivism

**Interpretivism** is a type of research methodology that focuses on how people interpret things and how this influences their behaviour. [https://en.wikipedia.org/wiki/Interpretivism\_(social_science)](<https://en.wikipedia.org/wiki/Interpretivism_(social_science)>)

**Positivism** is a philosophical theory stating that certain ("positive") knowledge is based on natural phenomena and their properties and relations. [https://en.wikipedia.org/wiki/Positivism](https://en.wikipedia.org/wiki/Positivism)

> Positivism is the scientific method that is used in data science and is also known as quantitative research.

## Operationalization

**Operationalization** is the process of strictly defining variables into measurable factors. [https://en.wikipedia.org/wiki/Operationalization](https://en.wikipedia.org/wiki/Operationalization)

## Book: "Students Guide to Writing College Papers"

Read chapter four and five of the book "Students Guide to Writing College Papers" by Kate L. Turabian.

Questions:

1. Consider the note: _"In computing, ACM and IEEE are the most frequently used styles."_

   - Which one should we actually use?
   - Is the choice based upon the type of paper we are writing? Or personal preference? Or something else?
   - **Answer:** Personal preference, but do it properly and consistently.

2. Consider the note: _"Data science is a new field, and publications may sometimes be scarce. Therefore, we often have to use blogs from reputable data scientists as sources. Tertiary sources may therefore play a larger role for us."_

   - What is an example of a reputable data scientist blog?
   - What is a tertiary source? What is an example of a tertiary source?
   - **Answer:** [https://www.kdnuggets.com/](https://www.kdnuggets.com/)

3. Consider the note _"Make sure that these three guidelines become a habit when reporting research results. Well-meant oversimplification is a notorious and disastrous source of misinformation. Always think about potential consequences of your writing. Maybe readers will do stupid things if they draw conclusions from, and take action based on, oversimplified information."_
   - Would the latter be the world we are living in? The world of social media we are living in?
   - **Answer:** Yes, it is the world we are living in. Only follow links on social media instead of citing. You should never cite social media posts.
