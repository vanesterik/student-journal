---
date: 2023-12-04T10:00:00+02:00
title: Storytelling with Data
---

| Lecturers                                    |
| -------------------------------------------- |
| [Maya Sappelli](mailto:maya.sappelli@han.nl) |

## Data Visualization and Communication

Help people to get to the top of the knowledge pyramid in order to convey your message.

### Science Literacy

Science literacy is the knowledge and understanding of scientific concepts and processes required for personal decision making, participation in civic and cultural affairs, and economic productivity. [Wikipedia](https://en.wikipedia.org/wiki/Science_literacy)

### Goals of Science Communication

Goals of science communication include:

- Aware
- Enjoyment
- Interest
- Opinion
- Understanding

### SQUACK method

- Suggestions, a comment or idea based on personal experience
- Questions, an area which needs clarity or exploration
- User Signal, data, feedback, or research that spurs a compelling question or authentic **Kudo**
- Accidental, a typo, copy and paste error, misalignment or math mistake
- Critical, problem that must be resolved because of the potential business risk or customer pain
- Kudos. praise of gratitude
