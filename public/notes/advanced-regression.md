---
date: 2023-11-06T10:00:00+02:00
title: Advanced Regression
---

| Lecturers                                     |
| --------------------------------------------- |
| [Onno Huijgen](mailto:onno.huijgen@han.nl)    |
| [Wim Wiegerinck](mailto:wim.wiegerink@han.nl) |

## Linear Regression

Linear Regression can be divided by three steps:

1. Use least squares to fit a line to the data
2. Calculate $R^2$
3. Calculate a $p$-value for $R^2$

### Least Squares

The **least squares** method is a method to find the best fitting line through a set of points. The best fitting line is the line that minimizes the sum of the squared distances from each point to the line. [https://en.wikipedia.org/wiki/Least_squares](https://en.wikipedia.org/wiki/Least_squares)

The formula for the **least squares** method is:

$$
y = a + bx
$$

Where $a$ is the intercept and $a$ is the slope.

> Fitting a line to data constitutes to finding a model.

### $R^2$

The $R^2$ is a measure of how well the line fits the data. The $R^2$ is a value between 0 and 1. The closer the $R^2$ is to 1 the better the line fits the data.
With this measurement one would like to figure out if the independent variable ($x$-axis) is able to explain the dependent variable ($y$-variable). [https://en.wikipedia.org/wiki/Coefficient_of_determination](https://en.wikipedia.org/wiki/Coefficient_of_determination)

The formula for $R^2$ is:

$$
R^2 = \frac{Var(mean)-Var(fit)}{Var(mean)}
$$

Where $Var$ is the variance of the data.

In the formula above we use the variance of the data. In order to calculate this you need to divide the sum of squares mean or fit by the number of sample size or $n$. For example:

$$
Var(mean) = \frac{(data-mean)^2}{n} = \frac{SS(mean)}{n}
$$

or in case of the fit:

$$
Var(fit) = \frac{(data-line)^2}{n} = \frac{SS(fit)}{n}
$$

Alternatively we could use the sum of squares directly and not make use of the variance formula. The formula for $R^2$ using the sum of squares is:

$$
R^2 = \frac{SS(mean)-SS(fit)}{SS(mean)}
$$

### $p$-value

The $p$-value is a measure of how likely it is that the $R^2$ is significant. The $p$-value is a value between 0 and 1. The closer the $p$-value is to 1 the more likely it is that the $R^2$ is significant. [https://en.wikipedia.org/wiki/P-value](https://en.wikipedia.org/wiki/P-value)

In order to approximate the $p$-value we need to calculate $F$ with the following formula:

$$
F = \frac{SS(mean)-SS(fit)/(p_{fit}-p_{mean})}{SS(fit)/(n-p_{fit})}
$$

... watch https://www.youtube.com/watch?v=vemZtEM63GY for more information ...

### Degrees of Freedom

The **degrees of freedom** is the number of values in the final calculation of a statistic that are free to vary. Basically how many knobs in a model can I term. [https://en.wikipedia.org/wiki/Degrees*of_freedom*(statistics)](<https://en.wikipedia.org/wiki/Degrees_of_freedom_(statistics)>)

## Questions

### Advanced Regression

1. If VIF-method where we regress say 10 variables against eachother gives a sufficiently high R-squared (>0.9) we can drop a variable, does it matter what variable, how do you decide which one?

   - **Answer:** You should start with the first suggested variable and work your way through them one at the time.

2. At one point in the example notebook we see that $R^2$ is 1 and $P>|t|$ value is large for both x1 and x2, we drop x2 and result is still good ($R^2$ still 1). Would you still drop x2 if $R^2$ dropped a bit, for instance from 1 to 0.95.

   - **Answer:** Yes, because one would estimate if the work is worth the effort. In case of very little change in the $R^2$ then one could remove this targeted parameter.

3. So I get that comparing simple linear vs multiple regression makes sense and can be used in feature selection. How does it compare to Regularization, would regularization fix these collinear issues as well?

   - **Answer:** Yes, because your model will be rewarded when removing unnecessary variables. Better to have a simpler model given that it is still correct in its predictions.

4. Could you perhaps narrate with broad strokes how this all ties into a data science project?

   - **Answer:** Following a typical data science project:
     1. Explore the data - ie. inspect outliers
     2. Clean the data - ie. remove NaN values
     3. Find correlations between variables
     4. Apply linear regression to find a model (this is the baseline model)
     5. Look for other possible models (beside linear regression) and compare
     6. Report your findings

5. In the reader states that using an **hold-out** sample leaves you subject to some uncertainty. In your experience is this uncertainty a big problem?

   - **Answer:** Not if there is enough data to work with. If there is not enough data this would be a problem as you would not be able to train your model properly.
