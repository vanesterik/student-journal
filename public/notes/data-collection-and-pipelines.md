---
date: 2023-09-18T10:00:00+02:00
title: Data Collection and Pipelines
---

| Lecturers                                                  |
| ---------------------------------------------------------- |
| [Ilona Wilmont](mailto:ilona.wilmont@han.nl)               |
| [Raoul Grouls](mailto:raoul.grouls@han.nl)                 |
| [Stijn Hoppenbrouwers](mailto:stijn.hoppenbrouwers@han.nl) |

## Data Scientist

A proper data scientist need to posses the following skills:

#### Hard skills

- Mathematics / Technology
- Execution
- Communication

#### Soft skills

- Rigor
- Ethics
- Positivity

Last couple weeks students struggled with the execution of the aforementioned list. This can be divided into three layers:

- Hardware
- Operating System
- Python
- Dependencies

One element of being able to execute is being able to work in the terminal.

## Statistics

Statistics can be divided into three layers **quantity**, **quality** and **biases**.

### Quantity

- Having too little data can be defined as:
  - At least 50 to 500 data points
  - [Bayes theorem](https://en.wikipedia.org/wiki/Bayes%27_theorem) could help when having too little data points
- Having too much data can be defined as:
  - For example having too many [features](<https://en.wikipedia.org/wiki/Feature_(machine_learning)>) within a data frame
  - [Feature Selection](https://en.wikipedia.org/wiki/Feature_selection)
  - [Regularization](<https://en.wikipedia.org/wiki/Regularization_(mathematics)>) - ie. web performance budget
  - Mathematical equational approach could be: 10 rows per feature

### Quality

- **Data pollution**, also known as data contamination, occurs when a dataset is compromised by the presence of incorrect, inconsistent, or irrelevant information, often introduced due to human error, system glitches, or malicious intent. It can severely impact the reliability and quality of data, leading to erroneous insights and potentially harmful decision-making if not properly addressed and cleaned.
- **Epistemic noise** refers to irrelevant or extraneous information that interferes with the process of acquiring or transmitting knowledge. It can encompass factors like bias, misinformation, distractions, or inaccuracies in data or communication, making it challenging to obtain accurate and reliable information or make informed decisions.
- **Aleatoric noise**, also known as random noise, is a type of uncertainty in data that arises from inherent randomness or unpredictability in a system or process. Unlike epistemic noise, which stems from errors or inaccuracies, aleatoric noise is typically beyond one's control and can only be addressed through statistical modeling or techniques that account for this inherent variability when making predictions or inferences.

### Biases

- [Sample bias](https://en.wikipedia.org/wiki/Sampling_bias)
- **Silent evidence** refers to relevant but unobservable data or information that is not readily apparent or taken into account in a given analysis or decision-making process. It underscores the importance of considering all available data, even if not immediately visible, to avoid biased conclusions or incomplete assessments.
- [Domain Knowledge](https://en.wikipedia.org/wiki/Domain_knowledge) could be applied in order to identify potential biases

## Pipelines

A pipeline is a set of data processing elements connected in series, where the output of one element is the input of the next one. The elements of a pipeline are often executed in parallel or in time-sliced fashion. Some amount of buffer storage is often inserted between elements.

### [Pydantic](https://pydantic-docs.helpmanual.io/)

Pydantic is a library that enforces type hints at runtime, and provides user friendly errors when data is invalid.

### References

- [Data Pipeline](https://en.wikipedia.org/wiki/Data_pipeline)
- [Scikit Learn](https://scikit-learn.org/stable/index.html)
