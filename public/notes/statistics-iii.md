---
date: 2023-10-23T10:00:00+02:00
title: Statistics III
---

| Lecturers                                    |
| -------------------------------------------- |
| [Ilona Wilmont](mailto:ilona.wilmont@han.nl) |
| [Raoul Grouls](mailto:raoul.grouls@han.nl)   |

## Binomial Distribution

In the restaurant example [A Bayesian Way of Choosing a Restaurant](https://towardsdatascience.com/a-bayesian-way-of-choosing-a-restaurant-87905a745854) there is an instance of a binomial distribution. The binomial distribution is a discrete probability distribution that describes the probability of $k$ successes in $n$ trials, given a success probability $p$ for each trial.

Somewhere in the example the author adds 1 to all the values in order to include a posterior distribution. This is also known as the [conjugate prior](https://en.wikipedia.org/wiki/Conjugate_prior).

## Frequentist vs Bayesian approach

The key difference is how they handle parameters: Frequentists treat them as fixed but unknown values, and Bayesians treat them as having probability distributions that change with new data. Frequentists rely on data alone, while Bayesians combine prior beliefs with data in a formal way.

In simple terms, frequentists are like detectives who believe in a single truth that they aim to discover from the evidence, while Bayesians are like scientists who have beliefs, gather evidence, and update their beliefs as they go along.

### Frequentist Approach

Imagine you're a frequentist, and you want to make conclusions based on data. In this approach, you treat the parameters of your model (like the average height of people in a city) as fixed, unchanging values. You believe that these parameters have a true, but unknown, value. To estimate these parameters, you use only the data you have and the likelihood of seeing the data given those parameters. You calculate things like sample means and variances.

For example, if you wanted to estimate the average height of people in a city, you'd collect a sample of heights and calculate the sample mean. The result is your best guess at the average height, and you provide a confidence interval to express your level of certainty.

### Bayesian Approach

Now, let's switch to the Bayesian approach. In this case, you see parameters as having probability distributions. You start with prior beliefs about these parameters based on existing knowledge or opinions. Then, as you collect data, you update these beliefs using Bayes' theorem. In simple terms, you update your beliefs based on the new evidence (data) you have.

Continuing with the height example, you might start with a prior belief about the average height in the city. As you collect height data, you use Bayes' theorem to adjust your belief, and your estimate becomes a probability distribution, not just a point estimate. You express your conclusions as probability distributions or posteriors.

## Mathematical Formulas

There are five base patterns for mathematical formulas that will be used when analyzing data:

- [Exponential function](https://en.wikipedia.org/wiki/Exponential_function) - ie. used for growth rates, compound interest, etc.
- [Linear function](https://en.wikipedia.org/wiki/Linear_function)
- [Logistic function](https://en.wikipedia.org/wiki/Logistic_function) - ie. used for death rates, sales, etc.
- [Polynomial](https://en.wikipedia.org/wiki/Polynomial)
- [Sine and Cosine](https://en.wikipedia.org/wiki/Sine)

## Random Walk

A random walk is a mathematical object, known as a stochastic or random process, that describes a path that consists of a succession of random steps on some mathematical space such as the integers.

[Random Walk](https://en.wikipedia.org/wiki/Random_walk)

## Other topics

### Using priors

Choosing a prior is not critical as with more data coming in - one would arrive at the same conclusion when choosing an alternative prior. However, it is important to choose a prior that fits the distribution - ie. when using a normal distribution, the prior should also be positive otherwise the model malfunctions.

### Traveling Salesmen Problem

The traveling salesman problem (TSP) asks the following question: "Given a list of cities and the distances between each pair of cities, what is the shortest possible route that visits each city exactly once and returns to the origin city?"

[Traveling Salesmen Problem](https://en.wikipedia.org/wiki/Travelling_salesman_problem)
