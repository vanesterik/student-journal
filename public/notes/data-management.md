---
date: 2023-09-25T10:00:00+02:00
title: Data Management
---

| Lecturers                                                  |
| ---------------------------------------------------------- |
| [Raoul Grouls](mailto:raoul.grouls@han.nl)                 |
| [Stijn Hoppenbrouwers](mailto:stijn.hoppenbrouwers@han.nl) |

No notes available yet.
