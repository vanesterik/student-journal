---
date: 2023-11-20T10:00:00+02:00
title: Advanced Clustering
---

| Lecturers                                     |
| --------------------------------------------- |
| [Onno Huijgen](mailto:onno.huijgen@han.nl)    |
| [Wim Wiegerinck](mailto:wim.wiegerink@han.nl) |

## Clustering

**Clustering** is the task of grouping a set of objects in such a way that objects in the same group (called a cluster) are more similar (in some sense) to each other than to those in other groups (clusters). [Wikipedia](https://en.wikipedia.org/wiki/Cluster_analysis)

Applications of clustering:

- **Anomaly Detection**: any instance that have low affinity to all clusters is likely to be an outlier.
- **Data Analysis**: it might be useful to cluster the instance and analyse each separately.
- **Dimensionality Reduction**: by replacing the features with each instance's affinity to each cluster.
- **Image segmentation**: by clustering pixels according to their color, then replacing pixel colors with the mean of its cluster.
- **Image similarity search**: clustering available images & when a new item is provided by the user, cluster it using the same algorithm and return the top N centered items.
- **Semi-supervised Learning**: If we have a few labels, we can perform clustering and propagate the available labels to other instances within the clusters.

### K-means Clustering

**K-means Clustering** is a method of vector quantization, originally from signal processing, that aims to partition n observations into k clusters in which each observation belongs to the cluster with the nearest mean (cluster centers or cluster centroid), serving as a prototype of the cluster. [Wikipedia](https://en.wikipedia.org/wiki/K-means_clustering), [Youtube](https://www.youtube.com/watch?v=4b5d3muPQmA), [Scikit-learn](https://scikit-learn.org/stable/modules/clustering.html#k-means)

**Merits**

- Simple to implement
- Scales to large data sets
- Guarantees convergence
- Can warm-start the positions of centroids

**Pitfalls**

- Can converge to a local optimum
- Can be slow for large numbers of samples

**Remedies**

- Restart the algorithm with different initializations of the centroids
- Use `n_init` parameter to set the number of initializations
- Use `max_iter` parameter to set the maximum number of iterations

### Determine optimal amount of clusters

**Elbow Method** is a method of interpretation and validation of consistency within cluster analysis designed to help finding the appropriate number of clusters in a dataset. [Wikipedia](<https://en.wikipedia.org/wiki/Elbow_method_(clustering)>), [Youtube](https://www.youtube.com/watch?v=9D915QxlCXY)

**Silhouette Method** is a method of interpretation and validation of consistency within cluster analysis designed to help finding the appropriate number of clusters in a dataset. [Wikipedia](<https://en.wikipedia.org/wiki/Silhouette_(clustering)>), [Youtube](https://www.youtube.com/watch?v=9D915QxlCXY)

### DBSCAN Clustering

**DBSCAN Clustering** is a density-based clustering algorithm: given a set of points in some space, it groups together points that are closely packed together (points with many nearby neighbors), marking as outliers points that lie alone in low-density regions (whose nearest neighbors are too far away). [Wikipedia](https://en.wikipedia.org/wiki/DBSCAN), [Youtube](https://www.youtube.com/watch?v=RDZUdRSDOok), [Scikit-learn](https://scikit-learn.org/stable/modules/clustering.html#dbscan)

**Merits**

- Can find arbitrarily shaped clusters
- Can handle clusters of different shapes and sizes
- Can handle noise

**Pitfalls**

- Can be slow for large data sets
- Can be sensitive to the parameter settings

**Remedies**

- Use `eps` parameter to set the maximum distance between two samples for them to be considered as in the same neighborhood
- Use `min_samples` parameter to set the number of samples in a neighborhood for a point to be considered as a core point
- Use `metric` parameter to set the distance metric to use for the tree
- Use `algorithm` parameter to set the algorithm to be used by the NearestNeighbors module to compute pointwise distances and find nearest neighbors

### Gaussian Mixture Models

**Gaussian Mixture Models** are a probabilistic model for representing normally distributed subpopulations within an overall population. [Wikipedia](https://en.wikipedia.org/wiki/Mixture_model), [Youtube](https://www.youtube.com/watch?v=q71Niz856KE), [Scikit-learn](https://scikit-learn.org/stable/modules/mixture.html#gmm)

**Merits**

- Can find arbitrarily shaped clusters
- Can handle clusters of different shapes and sizes
- Can handle noise

**Pitfalls**

- Can be slow for large data sets
- Can be sensitive to the parameter settings

**Remedies**

- Use `n_components` parameter to set the number of mixture components
- Use `covariance_type` parameter to set the type of covariance parameters to use
- Use `max_iter` parameter to set the number of EM iterations to perform
- Use `init_params` parameter to set the method used to initialize the weights, the means and the precisions

### Other Clustering Methods

Below a list of other clustering methods that are mentioned:

**Hierarchical Clustering** (Agglomerative Clustering) is a method of cluster analysis which seeks to build a hierarchy of clusters. [Wikipedia](https://en.wikipedia.org/wiki/Hierarchical_clustering), [Youtube](https://www.youtube.com/watch?v=7xHsRkOdVwo)

**Spectral Clustering** is a technique for clustering points in a high dimensional space. [https://en.wikipedia.org/wiki/Spectral_clustering](https://en.wikipedia.org/wiki/Spectral_clustering)

**Affinity Propagation** is a clustering algorithm based on the concept of "message passing" between data points. [https://en.wikipedia.org/wiki/Affinity_propagation](https://en.wikipedia.org/wiki/Affinity_propagation)

**Mean Shift** is a non-parametric feature-space analysis technique for locating the maxima of a density function, a so-called mode-seeking algorithm. [https://en.wikipedia.org/wiki/Mean_shift](https://en.wikipedia.org/wiki/Mean_shift)

**BIRCH** is a clustering algorithm that is designed to cluster large amounts of data by first generating a compact summary of the the data called a **clustering feature tree** (CFT), which is a tree-structured data, similar to a B-tree. [https://en.wikipedia.org/wiki/BIRCH](https://en.wikipedia.org/wiki/BIRCH)

### Maximum Likelihood

**Maximum Likelihood** is a method of estimating the parameters of a probability distribution by maximizing a likelihood function, so that under the assumed statistical model the observed data is most probable. [https://en.wikipedia.org/wiki/Maximum_likelihood_estimation](https://en.wikipedia.org/wiki/Maximum_likelihood_estimation), [https://www.youtube.com/watch?v=XepXtl9YKwc](https://www.youtube.com/watch?v=XepXtl9YKwc)

> Probability deals with the chance of an outcome given model parameters, while likelihood assesses how well observed data supports specific model parameter values.

> Maximum likelihood offers a structured method in order to define which model to use given a dataset.

### Convex Function and Non-Convex Function

**Convex Function** is a real-valued function defined on an interval with the property that its epigraph (the set of points on or above the graph of the function) is a convex set. [https://en.wikipedia.org/wiki/Convex_function](https://en.wikipedia.org/wiki/Convex_function)

**Non-Convex Function** is a real-valued function that is not convex. [https://en.wikipedia.org/wiki/Convex_function](https://en.wikipedia.org/wiki/Convex_function)

### Derivative

**Derivative** is a measure of how a function changes as its input changes. [https://en.wikipedia.org/wiki/Derivative](https://en.wikipedia.org/wiki/Derivative), [https://www.youtube.com/watch?v=ANyVpMS3HL4](https://www.youtube.com/watch?v=ANyVpMS3HL4)

### Expectation Maximization

**Expectation Maximization** is an iterative method for finding maximum likelihood or maximum a posteriori (MAP) estimates of parameters in statistical models, where the model depends on unobserved latent variables. [https://en.wikipedia.org/wiki/Expectation%E2%80%93maximization_algorithm](https://en.wikipedia.org/wiki/Expectation%E2%80%93maximization_algorithm), [https://www.youtube.com/watch?v=iQoXFmbXRJA](https://www.youtube.com/watch?v=iQoXFmbXRJA)

### Base Rules of Probability

**Base Rules of Probability** are a set of rules that can be used to calculate the probability of an event, given the probabilities of related events. [https://en.wikipedia.org/wiki/Probability_axioms](https://en.wikipedia.org/wiki/Probability_axioms)

Equation for the **Probability of an Event** is:

$$
P(\theta|X) = \frac{P(X|\theta)P(\theta)}{P(X)}
$$

One would like to know the probability of a hypothesis given the data. This is called the **posterior probability**. The posterior probability is proportional to the product of the **likelihood** and the **prior probability**. In short you would like to find out $\theta$ given $X$.

### Bayes' Theorem

**Bayes' Theorem** describes the probability of an event, based on prior knowledge of conditions that might be related to the event. [https://en.wikipedia.org/wiki/Bayes%27_theorem](https://en.wikipedia.org/wiki/Bayes%27_theorem), [https://www.youtube.com/watch?v=HZGCoVF3YvM](https://www.youtube.com/watch?v=HZGCoVF3YvM)

## Ethical Dilemmas

- [Nicomachean Ethics](https://en.wikipedia.org/wiki/Nicomachean_Ethics)
- [Immanuel Kant](https://en.wikipedia.org/wiki/Immanuel_Kant)
