---
date: 2023-11-27T10:00:00+02:00
title: Statistics of Ethics
---

| Lecturers                                      |
| ---------------------------------------------- |
| [Raoul Grouls](mailto:raoul.grouls@han.nl)     |
| [Vincent Wiegel](mailto:vincent.wiegel@han.nl) |

## Statistics of Ethics

### The Definition of a Graph

A graph $G$ is a pair:

$$
G = (V,E)
$$

Where $V$ is a set of vertices and $E$ is a set of edges between pairs of nodes.
Essentially they are items connected by some relation.

### Statistical Symbols

We use the symbol $\perp$ for the notion of statistical independence, E.g. $M =$ meteorites and $D =$ dinosaurs, then $M \perp D$ means that meteorites and dinosaurs are independent.

... read paper to extend example

### Counterfactual predictive parity

Counterfactual predictive parity is a fairness definition that is based on the notion of a counterfactual. A counterfactual is a statement about what would have happened if something had been different. For example, a counterfactual statement about a person who has been diagnosed with cancer is that the person would have been diagnosed with cancer if they had not been diagnosed with cancer. A counterfactual statement about a person who has been diagnosed with cancer is that the person would have been diagnosed with cancer if they had not been diagnosed with cancer. A counterfactual statement about a person who has been diagnosed with cancer is that the person would have been diagnosed with cancer if they had not been diagnosed with cancer. [Wikipedia](<https://en.wikipedia.org/wiki/Fairness_(machine_learning)>)

$$
Y(1) \perp A|D=0
$$

### Counterfactual equalized odds

Equalized odds,[1] also referred to as conditional procedure accuracy equality and disparate mistreatment, is a measure of fairness in machine learning. [WikiPedia](https://en.wikipedia.org/wiki/Equalized_odds)

$$
D \perp A|Y
$$

### Conditional principal fairness

Conditional principal fairness is a fairness definition that is based on the notion of a principal stratification. Principal stratification is a concept that was introduced by Frangakis and Rubin (2002) in the context of causal inference. It is a generalization of the concept of a subgroup, which is a subset of the population that is defined by a certain characteristic. Principal stratification is a partition of the population into mutually exclusive and exhaustive groups, where each group is defined by a certain characteristic. The characteristic that defines a group is called a principal stratum. The principal strata are defined in such a way that the causal effect of the treatment on the outcome is the same within each principal stratum. In other words, the principal strata are defined in such a way that the treatment does not have a causal effect on the outcome within each principal stratum. [Arxiv](https://arxiv.org/pdf/2005.10400v1.pdf)

$$
D \perp A|Y(0),Y(1)W
$$

### Counterfactual fairness

Counterfactual fairness is a fairness definition that is based on the notion of a counterfactual. A counterfactual is a statement about what would have happened if something had been different. For example, a counterfactual statement about a person who has been diagnosed with cancer is that the person would have been diagnosed with cancer if they had not been diagnosed with cancer. A counterfactual statement about a person who has been diagnosed with cancer is that the person would have been diagnosed with cancer if they had not been diagnosed with cancer. [Wikipedia](<https://en.wikipedia.org/wiki/Fairness_(machine_learning)>)

$$
\bold E[D(a^1)|X] = \bold E[D|X]
$$

### Max of Utility

Max of Utility is a fairness definition that is based on the notion of a utility function. A utility function is a function that assigns a value to each possible outcome of a decision. For example, a utility function for a decision about whether to buy a car might assign a value to each possible outcome of the decision, such as the value of the car, the value of the money spent on the car, and the value of the time spent on the car. [Wikipedia](<https://en.wikipedia.org/wiki/Fairness_(machine_learning)>)

### Causal Notions of Fairness

### Causal Direct Acyclic Graphs

In mathematics, particularly graph theory, and computer science, a directed acyclic graph (DAG) is a directed graph with no directed cycles. That is, it consists of vertices and edges (also called arcs), with each edge directed from one vertex to another, such that following those directions will never form a closed loop. [Wikipedia](https://en.wikipedia.org/wiki/Directed_acyclic_graph)

### Constructing a Causal DAG

One can take the following steps to construct a causal DAG:

1. Identify the variables of interest
2. Identify the causal relationships between the variables
3. Identify the confounders
4. Draw the causal DAG

### Features of Causal DAGs

- Causal DAGs are directed
- Causal DAGs are acyclic

### Be able to read the definitions 1-4 from the formula

### Understand the intuition of definition 5 (Path-specific fairness)

### Pareto dominance

Pareto dominance is a concept that is often used in decision-making to compare different options or policies. In simple terms, one option is said to dominate another if it is at least as good in all aspects and better in at least one aspect.

Imagine you have two choices, A and B. If A is better than B in some way and no worse in any other way, or if A is better in at least one way and equal in all others, then A is said to dominate B. On the other hand, if B is worse in every aspect compared to A, then B is said to be dominated by A.

Pareto dominance helps us identify options that are improvements or at least not worse in every aspect, making it a useful concept for decision-making when we want to find the best possible choices.

Pareto dominance is a concept that was introduced by Vilfredo Pareto in the context of economics. It is a generalization of the concept of a Pareto improvement, which is a change that makes at least one person better off without making anyone else worse off. [Wikipedia](https://en.wikipedia.org/wiki/Pareto_dominance)

### Understand the conclusion about the effects of enforcing various causal ## fairness definitions

### Be able to reason & argue about possible alternatives to enforcing causal fairness definitions by connecting and contrasting the algorithmic approach with the different ethical approaches like virtue ethics and utilitarianism

## Ethical Considerations

### "Given that you see"

For instance, imagine a marketing director at a department store who asks, “How likely is a customer who bought toothpaste to also buy dental floss?” Such questions are the bread and butter of statistics, and they are answered, first and foremost, by collecting and analyzing data. In our case, the question can be answered by first taking the data consisting of the shopping behavior of all customers, selecting only those who bought toothpaste, and, focusing on the latter group, computing the proportion who also bought dental floss. This proportion, also known as a “conditional probability,” measures (for large data) the degree of association between “buying toothpaste” and “buying floss.” Symbolically, we can write it as $P(floss | toothpaste)$. The “P” stands for “probability,” and the vertical line means “given that you see.”

### "What if we do?"

As these examples illustrate, the defining query of the second rung of the Ladder of Causation is “What if we do...?” What will happen if we change the environment? We can write this kind of query as $P(floss | do(toothpaste))$, which asks about the probability that we will sell floss at a certain price, given that we set the price of toothpaste at another price.

### Dating App Breeze Must and Can Adjust Algorithm to Prevent Discrimination

Read this [article](https://www.mensenrechten.nl/actueel/nieuws/2023/09/06/dating-app-breeze-mag-en-moet-algoritme-aanpassen-om-discriminatie-te-voorkomen) and identify the variables (explicit and implicit) in the article that are relevant and create a causal diagram:

![Casual Diagram](/images/causal-diagram.drawio.svg)

### Partially Observable Markov Decision Process

A partially observable Markov decision process is a generalization of a Markov decision process. A POMDP models an agent decision process in which it is assumed that the system dynamics are determined by an MDP, but the agent cannot directly observe the underlying state. Instead, it must maintain a sensor model and the underlying MDP. [Wikipedia](https://en.wikipedia.org/wiki/Partially_observable_Markov_decision_process)

#### Crying Baby Problem

The crying baby problem is a problem that is often used to illustrate the concept of a partially observable Markov decision process. It is a problem that is often used to illustrate the concept of a partially observable Markov decision process. [Wikipedia](https://en.wikipedia.org/wiki/Partially_observable_Markov_decision_process)
