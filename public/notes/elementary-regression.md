---
date: 2023-10-30T10:00:00+02:00
title: Elementary Regression
---

| Lecturers                                        |
| ------------------------------------------------ |
| [Onno Huijgen](mailto:onno.huijgen@han.nl)       |
| [Theo Theunissen](mailto:theo.theunissen@han.nl) |
| [Wim Wiegerinck](mailto:wim.wiegerink@han.nl)    |

## Linear Regression

**Linear Regression** is a linear approach to modeling the relationship between a scalar response and one or more explanatory variables. [https://en.wikipedia.org/wiki/Linear_regression](https://en.wikipedia.org/wiki/Linear_regression)

### Log normal Distribution

**Log-normal Distribution** is a continuous probability distribution of a random variable whose logarithm is normally distributed. [https://en.wikipedia.org/wiki/Log-normal_distribution](https://en.wikipedia.org/wiki/Log-normal_distribution)

The formula for the **log normal distribution** is:

$$
y = ax^b\eta
$$

or applied to the logarithm:

$$
log(y) = log(a) + b⋅log(x) + log(\eta)
$$

> **Error** and **noise** could be considered to be the same thing, as in they both describe residuals. But one would use the term **error** when trying to fit a line. And one would use the term **noise** when adding it to a formula to make the result of this formula more realistic.

### $F$-statistic vs $t$-statistic

Look up everything about the $F$-statistic and $t$-statistic in the `simpleLinearRegression.ipynb` notebook ... study terms:

- $F$-statistic and $F$-test
- $t$-statistic and $t$-test

### Natural Logarithm

**Natural Logarithm** is the logarithm to the base e, where e is an irrational and transcendental constant approximately equal to 2.718281828459. [https://en.wikipedia.org/wiki/Natural_logarithm](https://en.wikipedia.org/wiki/Natural_logarithm)

### Exponential Function

**Exponential Function** is a function of the form f(x) = bx, where b is a positive real number, and in which the argument x occurs as an exponent. [https://en.wikipedia.org/wiki/Exponential_function](https://en.wikipedia.org/wiki/Exponential_function)

### Gaussian Distribution

**Gaussian Distribution** (aka Normal Distribution) is a continuous probability distribution that gives a good description of data that cluster around a mean. [https://en.wikipedia.org/wiki/Normal_distribution](https://en.wikipedia.org/wiki/Normal_distribution)

## Central Limit Theorem

The **Central Limit Theorem** states that the sampling distribution of the sample means approaches a normal distribution as the sample size gets larger — no matter what the shape of the population distribution. [https://www.investopedia.com/terms/c/central_limit_theorem.asp](https://www.investopedia.com/terms/c/central_limit_theorem.asp)

## Empirical Knowledge

Empirical knowledge refers to knowledge that is acquired through direct observation, sensory experience, or experimentation. It is based on evidence and observations of the physical world and is often contrasted with other forms of knowledge, such as a priori knowledge, which is derived from pure reason and does not rely on sensory experience.

Empirical knowledge is grounded in the idea that we can gain understanding and insight about the world by making observations, conducting experiments, and collecting data. This knowledge is typically considered to be contingent on the specific circumstances and conditions under which the observations or experiments were made.

Examples of empirical knowledge include:

1. Scientific knowledge: Scientific discoveries are often based on empirical research. Scientists use experiments and observations to gather data and draw conclusions about the natural world.

2. Everyday observations: Simple everyday observations like noticing that water boils at 100 degrees Celsius at sea level or that objects fall when dropped are examples of empirical knowledge.

3. Historical knowledge: Much of our understanding of history comes from empirical sources such as historical documents, artifacts, and archaeological findings.

4. Personal experience: Personal experiences and sensory perceptions, such as feeling the heat from a flame or tasting the sweetness of sugar, contribute to our empirical knowledge.

Empirical knowledge is a fundamental component of the scientific method and is essential for building our understanding of the physical and natural world. It provides a basis for making informed decisions and drawing conclusions about reality based on evidence and observation.

## Reasoning and Ethics - Preparation

1. What are consequences of todays (last weeks) AI topic for you and society?

   - Major premise: Social media is presenting us with seemingly factual information which is in fact not true.
   - Minor premise: Our democracy is based on factual information also provided in the public domain.
   - Conclusion: Social media is a threat to our democracy.

2. Do you consider the consequences good/bad, positive/negative, weak/strong? Motivate your answer.

   - I consider the consequences to be bad, negative and strong. If we can't trust the information we receive, we can't make informed decisions. This is a threat to our democracy.

3. What are assumptions, premisses, viewpoints are the consequences based on? Motivate your answer.

   - The consequences are based on the assumption that the public domain is a place where one can find factual information.

4. Which type of reasoning is applied? Motivate your answer.

   - The reasoning is based on abduction. The reasoning is based on the assumption that the public domain is a place where we can find information that is not influenced by commercial or political interests.

5. Which ethical perspectives are in place? Motivate your answer.

   - The ethical perspective is based on prioritizing the truth over commercial and political interests.

## Information & Ethics

What is the concept of information?

- How to make a difference (Math) ((Shannon and Weaver, 1949))
- How to cause something (Philosophy)

Syntax, Semantics and Pragmatics

- Syntax: rules without meaning
- Semantics: meaning
- Pragmatics: usage

### Deduction

An example of a reasoning scheme of deduction:

- Major premise: **all x are y**
- Minor premise: **z is x**
- Conclusion: **z is y**

### Research question

What are all the necessary and sufficient conditions for deciding ... ?

## Questions

### Elementary Regression

1. Is the form of questions in the `simpleLinearRegression.ipynb` notebook to be expected in the exam?

   - **Answer**: Yes, the questions in the notebook are a good indication of the type of questions you can expect in the exam. Additionally a test exam will be provided in order to get a better idea of the type of questions you can expect in the exam.

2. Consider the answer to the second question of the **P-values** chapter in the `simpleLinearRegression.ipynb` notebook:

   > With larger data sets, (assuming there is a relation $y = a x + b + \epsilon$), `res.pvalue` tends to get smaller.

   - Am I correct in assuming that the p-value gets smaller because there is in fact a correlation between the two variables? But in when there is no clear correlation, the p-value will be higher no matter how large the dataset?
   - **Answer**: Yes, you are correct. It kinda states in the question ... "assuming there is a relation $y = a x + b + \epsilon$".

### Reasoning and Ethics

1. Could I state that the difference between indication and abduction is to arrive at different types of conclusions:

   - with indication, you arrive at a conclusion based on a personal subjective truth
   - with abdication, you arrive at a conclusion based on a truth that is generally accepted

2. Consider the last part of the dictionary definition:

   > All three words are based on Latin ducere, meaning "to lead." The prefix de- means "from," and deduction derives from generally accepted statements or facts. The prefix in- means "to" or "toward," and induction leads you to a generalization. The prefix ab- means "away," and you take away the best explanation in abduction.

   - "From", "towards" and "away" from what? I don't understand this part.

## Other References

- [The Book of Why](https://en.wikipedia.org/wiki/The_Book_of_Why) - Judea Pearl
- [The Black Swan: The Impact of the Highly Improbable](https://en.wikipedia.org/wiki/The_Black_Swan:_The_Impact_of_the_Highly_Improbable) - Nassim Taleb
- [Falsifiability](https://en.wikipedia.org/wiki/Falsifiability) - Karl Popper
