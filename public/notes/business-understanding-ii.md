---
date: 2023-09-11T10:00:00+02:00
title: Business Understanding II
---

| Lecturers                                                  |
| ---------------------------------------------------------- |
| [Gerlince Oversluizen](mailto:gerlinde.oversluizen@han.nl) |
| [Vincent Wiegel](mailto:vincent.wiegel@han.nl)             |

Notes on data science and data science terminology.

### Data Drift

Data Drift occurs when the **circumstances** change on how data acquisition is done. Think of the following examples:

- Sensors on a machine that inputs have changed
- Software that is used to collect data has changed
- Data is collected from a different source

### Concept Drift

Concept Drift occurs when the rules of a concept changes - ie. the offside rule in football changes.

### Model Decay

Model Decay occurs when the **situation** where the model is based has changed (this could be the aforementioned changed offside rule) ie. the result of the model has changed due to the changed offside rule.

## MADS Scheme

- Body of Knowledge (BoK) consists of scientific literature as well as all other technical knowledge
- Scientific literature is accessible with HAN-Quest, Radboud University or Google Scholar

[Stijn Hoppenbrouwers](mailto:stijn.hoppenbrouwers@han.nl), [Vincent Wiegel](mailto:vincent.wiegel@han.nl)

## Research Questions

- What is a Problem? A problem is an inequality between a desired situation and the current situation.
- Rational Approach vs Scientific Approach

### Questions from the business

Project to predict the amount of and optimal locations of gathering points for plastic bottles in order to reduce the amount of plastic waste in the environment.

- Analyze problem - identify key terms:
  - What is plastic waste?
  - What is an optimal location?
  - What is a gathering point?
- Decompose problem:
  - How many gathering points currently are there?
  - Where are the current gathering points?
  - How much plastic waste is out there?
  - How to measure current plastic waste?
  - How to measure the effect of the project?
- Reference existing body of knowledge and theory:
  - What are the current methods of measuring plastic waste?
  - What are the publications on this topic?
