import { cx } from 'class-variance-authority'
import Markdown from 'react-markdown'
import rehypeKatex from 'rehype-katex'
import remarkGfm from 'remark-gfm'
import remarkMath from 'remark-math'

type ArticleProps = {
  content: string
}

export const Article = ({ content }: ArticleProps) => (
  <article
    className={cx(
      'prose',
      'prose-a:text-blue',
      'prose-blockquote:border-l-blue',
      'prose-headings:text-base01',
      'prose-img:rounded-lg',
      'prose-li:marker:text-base00',
      'prose-li:text-base00',
      'prose-p:text-base00',
      'prose-strong:text-base00',
      // Table
      'prose-table:border-base2',
      'prose-table:border',
      'prose-table:overflow-x-auto',
      'prose-thead:bg-base2',
      'prose-thead:border-none',
      'prose-th:p-2',
      'prose-th:font-normal',
      'prose-tr:border-base2',
      'prose-td:p-2',
      // Anchor
      'hover:prose-a:no-underline',
      // Miscellaneous
      'w-full',
    )}
  >
    <Markdown
      rehypePlugins={[rehypeKatex]}
      remarkPlugins={[remarkGfm, remarkMath]}
    >
      {content}
    </Markdown>
  </article>
)
