import { cx } from 'class-variance-authority'
import { PropsWithChildren } from 'react'

import { Stack } from '../Stack'

type HeadlineProps = PropsWithChildren<{
  date: Date
  title: string
}>

export const Headline = ({ children, date, title }: HeadlineProps) => (
  <Stack>
    <time className={cx('text-base1', 'text-sm')} dateTime={date.toISOString()}>
      {date.toDateString()}
    </time>
    <h1 className={cx('font-bold', 'text-4xl', 'text-base01')}>{title}</h1>
    {children}
  </Stack>
)
