import { cva, cx, type VariantProps } from 'class-variance-authority'
import Link from 'next/link'
import { ButtonHTMLAttributes, ComponentProps } from 'react'

const button = cva(
  cx(
    'border',
    'flex',
    'flex-row',
    'items-center',
    'gap-x-3',
    'px-4',
    'py-3',
    'rounded-lg',
    'hover:bg-blue',
    'hover:border-blue',
    'hover:text-base3',
  ),
  {
    variants: {
      variant: {
        default: ['border-base2'],
        ghost: ['border-transparent'],
      },
    },
    defaultVariants: {
      variant: 'default',
    },
  },
)

type ButtonProps = (
  | ComponentProps<typeof Link>
  | ButtonHTMLAttributes<HTMLButtonElement>
) &
  VariantProps<typeof button>

export const Button = ({ className, variant, ...rest }: ButtonProps) => {
  if ('href' in rest)
    return (
      <Link className={button({ className, variant })} {...rest}>
        {rest.children}
      </Link>
    )

  return (
    <button
      className={button({ className, variant })}
      {...(rest as ButtonHTMLAttributes<HTMLButtonElement>)}
    />
  )
}
