import { DSVRowArray } from 'd3'

export const parseNodes = (data: DSVRowArray<string>) => {
  const concepts = data.flatMap(({ source, target }) => [source, target])
  return Array.from(new Set(concepts), (id) => ({ id }))
}

export const parseLinks = (data: DSVRowArray<string>) =>
  data.map(({ source, relation, target }) => ({ source, relation, target }))
