import { csvParse } from 'd3'
import { describe, expect, it } from 'vitest'

import { parseLinks, parseNodes } from './helpers'

const data = csvParse('source,target\nfoo,bar\nfoo,bar\nbaz,qux')

describe('parseNodes', () => {
  it('should return object array unique entries', () => {
    expect(parseNodes(data)).toEqual([
      { id: 'foo' },
      { id: 'bar' },
      { id: 'baz' },
      { id: 'qux' },
    ])
  })
})

describe('parseLinks', () => {
  it('should return object array with source and target', () => {
    expect(parseLinks(data)).toEqual([
      { source: 'foo', target: 'bar' },
      { source: 'foo', target: 'bar' },
      { source: 'baz', target: 'qux' },
    ])
  })
})
