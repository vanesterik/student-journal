'use client'
/* eslint-disable @typescript-eslint/no-explicit-any */
import { cx } from 'class-variance-authority'
import * as d3 from 'd3'
import { useEffect, useRef } from 'react'

import { parseLinks, parseNodes } from './utils/helpers'

const DIAGRAM_WIDTH = 990
const DIAGRAM_HEIGHT = 660

export const Diagram = () => {
  const initialized = useRef(false)
  useEffect(() => {
    if (initialized.current) return
    initialized.current = true
    initialize()
  }, [])

  return (
    <svg
      className={cx('bg-base2', 'h-auto', 'max-w-full', 'rounded-lg')}
      width={DIAGRAM_WIDTH}
      height={DIAGRAM_HEIGHT}
    />
  )
}

const initialize = async () => {
  const onTick = () => {
    link
      .attr('x1', (d) => (d.source as any).x)
      .attr('y1', (d) => (d.source as any).y)
      .attr('x2', (d) => (d.target as any).x)
      .attr('y2', (d) => (d.target as any).y)

    edge.attr(
      'd',
      (d) =>
        `M${(d.source as any).x},${(d.source as any).y}L${
          (d.target as any).x
        },${(d.target as any).y}`,
    )

    edgeLabel.attr('transform', (d, index) => {
      if ((d.target as any).x < (d.source as any).x) {
        const text = d3.select(`#edge_label_${index}`)
        const box = (text.node() as any).getBBox()
        const rx = box.x + box.width / 2
        const ry = box.y + box.height / 2
        return `rotate(180 ${rx} ${ry})`
      } else {
        return 'rotate(0)'
      }
    })

    node.attr('transform', (d) => `translate(${(d as any).x},${(d as any).y})`)
  }

  const onDragStart = (event: any) => {
    if (!event.active) simulation.alphaTarget(0.3).restart()
    event.subject.fx = event.subject.x
    event.subject.fy = event.subject.y
  }

  const onDrag = (event: any) => {
    event.subject.fx = event.x
    event.subject.fy = event.y
  }

  const onDragEnd = (event: any) => {
    if (!event.active) simulation.alphaTarget(0)
    event.subject.fx = null
    event.subject.fy = null
  }

  const data = await d3.csv('data/concepts.csv')
  const nodes = parseNodes(data)
  const links = parseLinks(data)

  const simulation = d3
    .forceSimulation(nodes as any)
    .force(
      'link',
      d3
        .forceLink(links)
        .id((d: any) => d.id)
        .distance(120)
        .strength(1),
    )
    .force('charge', d3.forceManyBody().strength(-500))
    .force('center', d3.forceCenter(DIAGRAM_WIDTH / 2, DIAGRAM_HEIGHT / 2))
    .on('tick', onTick)

  const svg = d3.select('svg')

  const defs = svg.append('defs')

  defs
    .append('marker')
    .attr('id', 'arrow')
    .attr('viewBox', '0 -5 10 10')
    .attr('refX', '20')
    .attr('refY', '0')
    .attr('markerWidth', '7')
    .attr('markerHeight', '7')
    .attr('orient', 'auto')
    .append('path')
    .attr('class', 'fill-base00')
    .attr('d', 'M0,-5L10,0L0,5')

  const link = svg
    .append('g')
    .attr('class', 'stroke-base00')
    .selectAll()
    .data(links)
    .join('line')
    .attr('marker-end', 'url(#arrow)')

  const edge = svg
    .append('g')
    .selectAll()
    .data(links)
    .join('path')
    .attr('id', (_, index) => `edge_${index}`)

  const edgeLabel = svg
    .append('g')
    .selectAll()
    .data(links)
    .join('text')
    .attr('id', (_, index) => `edge_label_${index}`)
    .attr('paint-order', 'stroke')
    .attr('class', 'fill-base00 select-none stroke-3 stroke-base2 text-xs')

  edgeLabel
    .append('textPath')
    .attr('style', 'text-anchor: middle;')
    .attr('startOffset', '50%')
    .attr('dominant-baseline', 'middle')
    .attr('href', (_, index) => `#edge_${index}`)
    .text((d) => (d as any).relation)

  const node = svg
    .append('g')
    .attr('class', 'select-none')
    .selectAll()
    .data(nodes)
    .join('g')
    .call(
      (d3.drag() as any)
        .on('start', onDragStart)
        .on('drag', onDrag)
        .on('end', onDragEnd),
    )

  node
    .append('circle')
    .attr('r', 6)
    .attr('paint-order', 'stroke')
    .attr('class', 'fill-red stroke-3 stroke-base2')

  node
    .append('a')
    .attr(
      'href',
      (d) =>
        `https://en.wikipedia.org/wiki/${d.id.toLowerCase().replace(' ', '_')}`,
    )
    .attr('target', '_blank')
    .attr('class', 'no-underline')
    .append('text')
    .attr('x', 8)
    .attr('y', 5)
    .text((d) => d.id)
    .attr('paint-order', 'stroke')
    .attr('class', 'fill-base00 stroke-3 stroke-base2 text-sm')
}
