import { cx } from 'class-variance-authority'

import { Button } from '../Button'
import { Stack } from '../Stack'

type PaginationLink = {
  href: string
  title: string
}

type PaginationProps = {
  pagination: Array<PaginationLink>
}

export const Pagination = ({ pagination }: PaginationProps) => {
  const [previous, next] = pagination

  return (
    <div className={cx('grid', 'grid-cols-2', 'gap-x-4', 'w-full')}>
      <PreviousButton {...previous} />
      <NextButton {...next} />
    </div>
  )
}

const PreviousButton = ({ href, title }: PaginationLink) => (
  <Button href={href}>
    <ButtonArrow direction="left" />
    <ButtonCaption alignment="left" label="Previous" title={title} />
  </Button>
)

const NextButton = ({ href, title }: PaginationLink) => (
  <Button className={cx('justify-end')} href={href}>
    <ButtonCaption alignment="right" label="Next" title={title} />
    <ButtonArrow direction="right" />
  </Button>
)

type ButtonCaptionProps = Pick<PaginationLink, 'title'> & {
  alignment: 'left' | 'right'
  label: string
}

const ButtonCaption = ({ alignment, label, title }: ButtonCaptionProps) => (
  <Stack alignment={alignment} size="none">
    <span className={cx('text-xs')}>{label}</span>
    <span className={cx('font-bold')}>{title}</span>
  </Stack>
)

type ButtonArrowProps = {
  direction: 'left' | 'right'
}

const ButtonArrow = ({ direction }: ButtonArrowProps) => {
  const d =
    direction === 'left'
      ? 'M7.50481 17.1059L0.219809 9.5499C0.0786949 9.40156 0 9.20465 0 8.9999C0 8.79516 0.0786949 8.59825 0.219809 8.4499L7.50481 0.894905C7.57303 0.822884 7.6552 0.765522 7.74633 0.72632C7.83745 0.687119 7.93561 0.666901 8.03481 0.666901C8.13401 0.666901 8.23217 0.687119 8.32329 0.72632C8.41442 0.765522 8.49659 0.822884 8.56481 0.894905C8.70592 1.04325 8.78462 1.24016 8.78462 1.4449C8.78462 1.64965 8.70592 1.84656 8.56481 1.9949L2.56081 8.2219H19.2498C19.6638 8.2219 19.9998 8.5709 19.9998 8.9999C19.9998 9.4299 19.6638 9.7779 19.2498 9.7779H2.56081L8.56481 16.0059C8.70592 16.1543 8.78462 16.3512 8.78462 16.5559C8.78462 16.7606 8.70592 16.9576 8.56481 17.1059C8.49644 17.1777 8.41422 17.2348 8.32311 17.2738C8.23201 17.3129 8.13392 17.333 8.03481 17.333C7.93569 17.333 7.83761 17.3129 7.74651 17.2738C7.6554 17.2348 7.57317 17.1777 7.50481 17.1059Z'
      : 'M12.495.894 19.78 8.45a.798.798 0 0 1 0 1.1l-7.285 7.555a.73.73 0 0 1-1.06 0 .798.798 0 0 1 0-1.1l6.004-6.227H.75C.336 9.778 0 9.429 0 9c0-.43.336-.778.75-.778h16.689l-6.004-6.228a.798.798 0 0 1 0-1.1.732.732 0 0 1 1.06 0z'

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="18"
      viewBox="0 0 20 18"
    >
      <path fill="currentColor" d={d}></path>
    </svg>
  )
}
