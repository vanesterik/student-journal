import { cva, cx, type VariantProps } from 'class-variance-authority'
import { PropsWithChildren } from 'react'

const stack = cva(cx('flex'), {
  variants: {
    alignment: {
      left: ['items-start'],
      center: ['items-center'],
      right: ['items-end'],
    },
    orientation: {
      vertical: ['flex-col'],
      horizontal: ['flex-row'],
    },
    size: {
      none: ['gap-0'],
      sm: ['gap-2'],
      md: ['gap-4'],
      lg: ['gap-8'],
      xl: ['gap-16'],
    },
  },
  defaultVariants: {
    alignment: 'left',
    orientation: 'vertical',
    size: 'sm',
  },
})

type SectionProps = PropsWithChildren<VariantProps<typeof stack>>

export const Stack = ({
  alignment,
  children,
  orientation,
  size,
}: SectionProps) => (
  <div className={stack({ alignment, orientation, size })}>{children}</div>
)
