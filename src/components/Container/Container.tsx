import { cva, cx, VariantProps } from 'class-variance-authority'
import { PropsWithChildren } from 'react'

const container = cva(cx('max-w-2xl', 'mx-auto', 'px-4', 'w-full'), {
  variants: {
    size: {
      sm: 'max-w-md',
      md: 'max-w-2xl',
      lg: 'max-w-4xl',
    },
  },
  defaultVariants: {
    size: 'md',
  },
})

type ContainerProps = PropsWithChildren<
  {
    className?: string
    element?: 'div' | 'section' | 'article' | 'header' | 'footer'
  } & VariantProps<typeof container>
>

export const Container = ({
  children,
  className,
  element = 'div',
  size,
}: ContainerProps) => {
  const Element = element

  return (
    <Element className={container({ className, size })}>{children}</Element>
  )
}
