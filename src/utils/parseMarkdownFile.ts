import { readdirSync, readFileSync } from 'fs'
import matter from 'gray-matter'
import { join } from 'path'

/**
 * Parse all slugs by calling and processing `parseAllMarkdownFiles` function
 */
export const parseAllMarkdownSlugs = (folderPath: string, basePath: string) =>
  parseAllMarkdownFiles(folderPath, basePath).map(({ slug }) => ({ slug }))

/**
 * Parse all markdown files from folder containing markdown files
 */
export const parseAllMarkdownFiles = (folderPath: string, basePath: string) => {
  // Parse full path
  const fullPath = join(folderPath, basePath)
  // Read all filenames from folder
  const filenames = readdirSync(fullPath)
  // Process and return all files based on filename
  return (
    // eslint-disable-next-line fp/no-mutating-methods
    filenames
      // Parse single markdown file
      .map((filename) => ({
        ...parseMarkdownFile(join(fullPath, filename)),
        // Add slug based on filename
        slug: filename.replace(/\.md$/, ''),
      }))
      // Sort files by date ... unfortunately, we need to mutate the array due to
      // the use of `sort`, but as soon as `isSorted` is available in node-js -
      // we'll be sure to refactor this
      //
      .sort((a, b) => a.date.getTime() - b.date.getTime())
      // Add pagination to all files
      .map((file, index, files) => {
        // Get previous and next file
        const previous = files[(index - 1 + files.length) % files.length]
        const next = files[(index + 1) % files.length]
        // Return file with pagination prop
        return {
          ...file,
          pagination: [
            {
              href: join('/', basePath, previous.slug),
              title: previous.title,
            },
            {
              href: join('/', basePath, next.slug),
              title: next.title,
            },
          ],
        }
      })
  )
  // Sort files by date ... unfortunately, we need to mutate the array due to
  // the use of `sort`, but as soon as `isSorted` is available in node-js -
  // we'll be sure to refactor this
  //
  // eslint-disable-next-line fp/no-mutating-methods
  // return files.sort((a, b) => a.date.getTime() - b.date.getTime())
}

/**
 * Parse markdown file content and frontmatter. Returns object with content and
 * assigned frontmatter data
 */
export const parseMarkdownFile = (filePath: string) => {
  const data = readFileSync(filePath, 'utf8')
  const result = matter(data, { excerpt: true })

  return {
    content: result.content,
    excerpt: result.excerpt,
    date: result.data.date,
    title: result.data.title,
  }
}
