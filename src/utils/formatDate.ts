export const formatDate = (date: Date) => {
  const year = date.getFullYear().toString()
  const month = addLeadingZero(date.getMonth() + 1)
  const day = addLeadingZero(date.getDate())
  return [year, month, day].join('-')
}

const addLeadingZero = (number: number) => {
  const string = number.toString()
  return string.length === 1 ? `0${string}` : string
}
