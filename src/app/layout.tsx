import './globals.css'

import { cx } from 'class-variance-authority'
import { Poppins, Source_Code_Pro } from 'next/font/google'
import Link from 'next/link'
import { Fragment, ReactNode } from 'react'

import { Stack } from '@/components/Stack'

const poppins = Poppins({
  display: 'swap',
  subsets: ['latin'],
  variable: '--font-poppins',
  weight: ['400', '700'],
})

const sourceCodePro = Source_Code_Pro({
  display: 'swap',
  subsets: ['latin'],
  variable: '--font-source-code-pro',
  weight: ['400'],
})

export default function RootLayout({ children }: { children: ReactNode }) {
  return (
    <html lang="en" className={`${poppins.variable} ${sourceCodePro.variable}`}>
      <head>
        <link
          href="https://cdn.jsdelivr.net/npm/katex@0.16.8/dist/katex.min.css"
          rel="stylesheet"
        />
      </head>
      <body
        className={cx(
          'flex',
          'flex-col',
          'h-full',
          'min-h-screen',
          'max-w-none',
          'gap-y-16',
        )}
      >
        <header
          className={cx(
            'backdrop-blur-sm',
            'bg-base3/80',
            'border-b-base2',
            'border-b',
            'flex-row',
            'flex',
            'justify-between',
            'px-4',
            'py-2',
            'sticky',
            'top-0',
          )}
        >
          <Link
            className={cx(
              'font-mono',
              'text-base1',
              'text-sm',
              'hover:text-blue',
            )}
            href="/"
          >
            student_journal.kve
          </Link>
          <Stack orientation="horizontal">
            {[
              ['notes', '/notes'],
              ['source', 'https://gitlab.com/vanesterik/student-journal'],
            ].map(([title, href], index, array) => (
              <Fragment key={href}>
                <Link
                  className={cx(
                    'font-mono',
                    'text-base1',
                    'text-sm',
                    'hover:text-blue',
                  )}
                  href={href}
                >
                  {title}
                </Link>
                {index < array.length - 1 && (
                  <span className={cx('font-mono', 'text-sm')}>/</span>
                )}
              </Fragment>
            ))}
          </Stack>
        </header>
        <main className={cx('flex-1')}>{children}</main>
        <footer className={cx('mx-auto', 'px-4', 'py-2')}>
          <Link
            className={cx(
              'no-underline',
              'text-sm',
              'text-base1',
              'hover:text-blue',
              'hover:underline',
            )}
            href="https://gitlab.com/vanesterik/student-journal/-/blob/main/LICENSE"
          >
            &copy; 2023 Koen van Esterik
          </Link>
        </footer>
      </body>
    </html>
  )
}
