import { redirect } from 'next/navigation'

import { Article } from '@/components/Article'
import { Container } from '@/components/Container'
import { Headline } from '@/components/Headline'
import { Pagination } from '@/components/Pagination'
import { Stack } from '@/components/Stack'
import {
  parseAllMarkdownFiles,
  parseAllMarkdownSlugs,
} from '@/utils/parseMarkdownFile'

export function generateStaticParams() {
  return parseAllMarkdownSlugs('public', 'notes')
}

export default function NoteDetailPage({
  params: { slug },
}: {
  params: {
    slug: string
  }
}) {
  const files = parseAllMarkdownFiles('public', 'notes')
  const props = files.find((file) => file.slug === slug)

  // If the note doesn't exist, redirect to 404
  if (!props) redirect('/404')

  const { content, date, pagination, title } = props

  return (
    <Container>
      <Stack size="xl">
        <Headline date={date} title={title} />
        <Article content={content} />
        <Pagination pagination={pagination} />
      </Stack>
    </Container>
  )
}
