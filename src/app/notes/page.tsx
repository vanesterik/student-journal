import { cx } from 'class-variance-authority'
import Link from 'next/link'

import { Container } from '@/components/Container'
import { Stack } from '@/components/Stack'
import { formatDate } from '@/utils/formatDate'
import { parseAllMarkdownFiles } from '@/utils/parseMarkdownFile'

export default function NoteListPage() {
  const files = parseAllMarkdownFiles('public', 'notes')

  return (
    <Container>
      <Stack size="lg">
        <h1 className={cx('font-bold', 'text-4xl', 'text-base01')}>Notes</h1>
        <ul className={cx('flex', 'flex-col', 'gap-y-4', 'w-full')}>
          {files.map(({ date, slug: href, title }) => (
            <li
              className={cx('items-center', 'flex', 'flex-row', 'gap-x-2')}
              key={href}
            >
              <Link
                className={cx('hover:text-blue', 'hover:underline')}
                href={href}
              >
                {title}
              </Link>
              <div
                className={cx(
                  'border-b',
                  'border-b-base2',
                  'flex-grow',
                  'h-[1px]',
                )}
              />
              <time
                className={cx('font-mono', 'text-base1', 'text-sm')}
                dateTime={date.toISOString()}
              >
                {formatDate(date)}
              </time>
            </li>
          ))}
        </ul>
      </Stack>
    </Container>
  )
}
