import { cx } from 'class-variance-authority'

import { Article } from '@/components/Article'
import { Container } from '@/components/Container'
import { Diagram } from '@/components/Diagram'
import { Stack } from '@/components/Stack'

export default function Page() {
  return (
    <Stack size="lg">
      <Container>
        <Stack size="lg">
          <h1 className={cx('font-bold', 'text-7xl', 'text-base01')}>
            Master Applied Data Science
          </h1>
          <Article
            content={`
This is my student journal for the Master Applied Data Science at the HAN in
Arnhem in the years 23-25. Featuring college notes about various topics in
regards said master.`}
          />
        </Stack>
      </Container>
      <Container size="lg">
        <Diagram />
      </Container>
      <Container>
        <Article
          content={`
Above a concept diagram featuring my current understanding of various concepts
that are being covered. This diagram will be updated while progressing through
the master.
        
By dragging the nodes around you can explore the diagram. Clicking on a node
will trigger a corresponding wikipedia search request.`}
        />
      </Container>
    </Stack>
  )
}
