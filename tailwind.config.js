/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{ts,tsx}'],
  plugins: [require('@tailwindcss/typography')],
  theme: {
    colors: {
      base03: '#002b36',
      base02: '#073642',
      base01: '#586e75', // optional emphasized content
      base00: '#657b83', // body text / default code / primary content
      base0: '#839496',
      base1: '#93a1a1', // comments / secondary content
      base2: '#eee8d5', // background highlights
      base3: '#fdf6e3', // background
      yellow: '#b58900',
      orange: '#cb4b16',
      red: '#dc322f',
      magenta: '#d33682',
      violet: '#6c71c4',
      blue: '#268bd2',
      cyan: '#2aa198',
      green: '#859900',
    },
    fontFamily: {
      sans: ['var(--font-poppins)', 'sans-serif'],
      mono: ['var(--font-source-code-pro)', 'monospace'],
    },
    strokeWidth: {
      1: '1px',
      2: '2px',
      3: '3px',
    },
  },
}
